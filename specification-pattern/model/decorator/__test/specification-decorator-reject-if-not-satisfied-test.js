/* jshint node: true */

'use strict'

var Promise = require('bluebird')

var expect = require('chai').expect

var SpecificationRejectIfNotSatisfied = require('../specification-decorator-reject-if-is-not-satisfied')
  .Constructor

describe('Specification Pattern || Specification Decorator || Reject if not satisfied', function() {
  it('Create a reject if not satisfied spec', function(done) {
    var alwaysFalse = SpecificationRejectIfNotSatisfied(function() {
      return Promise.resolve(false)
    })

    alwaysFalse
      .isSatisfiedBy('whatever')
      .then(function() {
        done(new Error('MUST_NEVER_BE_CALLED'))
      })
      .catch(function(e) {
        expect(e).to.be.an('error')
        expect(e.message).to.be.equals('IS_NOT_SATISFIED_BY')
        done()
      })
  })
})
