/* jshint node: true */

'use strict'

var expect = require('chai').expect

var SpecificationDecorator = require('../specification-decorator').Constructor
var RejectIfNotSatisfiedDecorator = require('../specification-decorator-reject-if-is-not-satisfied').Constructor
var SpecificationRejectIfNotSatisfied = SpecificationDecorator([RejectIfNotSatisfiedDecorator]).Constructor

describe('Specification Pattern || Specification Decorator', function() {
  it('Create a reject if not satisfied spec', function(done) {
    var alwaysFalse = SpecificationRejectIfNotSatisfied(function() {
      return false
    })

    alwaysFalse
      .isSatisfiedBy('whatever')
      .then(function() {
        done(new Error('MUST_NEVER_BE_CALLED'))
      })
      .catch(function(e) {
        expect(e).to.be.an('error')
        expect(e.message).to.be.equals('IS_NOT_SATISFIED_BY')
        done()
      })
  })
})
