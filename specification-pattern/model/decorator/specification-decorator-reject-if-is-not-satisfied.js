/* jshint node: true */

'use strict'

///////////////////////////////////////////////////////
/////////// DEPENDENCIES
///////////////////////////////////////////////////////
var Promise = require('bluebird')

var PATH_TO_MODULE_FACTORY_PATTERN = '../../../module-factory-pattern'
var ModuleFactoryPattern = require(PATH_TO_MODULE_FACTORY_PATTERN + '/module-factory-pattern').Constructor()

var Specification = require('../specification').Constructor

///////////////////////////////////////////////////////
/////////// CONSTRUCTOR
///////////////////////////////////////////////////////
var SpecificationDecoratorRejectIfNotSatisfied = function(specOrIsSatisfiedBy) {
  var spec = null

  if (typeof specOrIsSatisfiedBy === 'function') {
    var isSatisfiedBy = specOrIsSatisfiedBy
    spec = Specification(isSatisfiedBy)
  } else {
    spec = specOrIsSatisfiedBy
  }

  var isSatisfiedByFunction = spec.isSatisfiedBy

  var decoratedIsSatisfiedByFunction = function(value) {
    var result = isSatisfiedByFunction.call(spec, value)
    if (result !== null && typeof result !== 'undefined' && typeof result.then === 'function') {
      var promise = result
      return promise.then(function(isSatisfiedBy) {
        return handleResult(isSatisfiedBy)
      })
    } else {
      var isSatisfiedBy = result
      return handleResult(isSatisfiedBy)
    }
  }
  spec.isSatisfiedBy = decoratedIsSatisfiedByFunction
  return spec
}

///////////////////////////////////////////////////////
/////////// EXPORTS
///////////////////////////////////////////////////////
module.exports = ModuleFactoryPattern.factory(SpecificationDecoratorRejectIfNotSatisfied)

///////////////////////////////////////////////////////
/////////// PRIVATE
///////////////////////////////////////////////////////
var handleResult = function(isSatisfiedBy) {
  if (!isSatisfiedBy) {
    return Promise.reject(new Error('IS_NOT_SATISFIED_BY'))
  }

  return Promise.resolve(isSatisfiedBy)
}
