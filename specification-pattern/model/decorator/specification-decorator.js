/* jshint node: true */

'use strict'

///////////////////////////////////////////////////////
/////////// DEPENDENCIES
///////////////////////////////////////////////////////
var PATH_TO_MODULE_FACTORY_PATTERN = '../../../module-factory-pattern'
var ModuleFactoryPattern = require(PATH_TO_MODULE_FACTORY_PATTERN + '/module-factory-pattern').Constructor()

var Specification = require('../specification').Constructor

///////////////////////////////////////////////////////
/////////// CONSTRUCTOR
///////////////////////////////////////////////////////
var SpecificationDecorator = function(decorators) {
  var validDecorators = decorators || []
  return {
    Constructor: function(isSatisfiedBy) {
      var spec = Specification(isSatisfiedBy)

      for (var decorator_index = 0; decorator_index < validDecorators.length; decorator_index++) {
        var decorator = validDecorators[decorator_index]
        spec = decorator(spec)
      }

      return spec
    }
  }
}

///////////////////////////////////////////////////////
/////////// EXPORTS
///////////////////////////////////////////////////////
module.exports = ModuleFactoryPattern.factory(SpecificationDecorator)

///////////////////////////////////////////////////////
/////////// MISC
///////////////////////////////////////////////////////
