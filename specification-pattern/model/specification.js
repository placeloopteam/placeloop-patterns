/* jshint node: true */

'use strict'

///////////////////////////////////////////////////////
/////////// DEPENDENCIES
///////////////////////////////////////////////////////
var PATH_TO_MODULE_FACTORY_PATTERN = '../../module-factory-pattern'
var ModuleFactoryPattern = require(PATH_TO_MODULE_FACTORY_PATTERN + '/module-factory-pattern').Constructor()

///////////////////////////////////////////////////////
/////////// CONSTRUCTOR
///////////////////////////////////////////////////////
var Specification = function(isSatisfiedBy) {
  var AndSpecification = require('./logic/and-specification').Constructor
  var OrSpecification = require('./logic/or-specification').Constructor
  var NotSpecification = require('./logic/not-specification').Constructor

  var validIsSatisfiedBy = typeof isSatisfiedBy === 'function' ? isSatisfiedBy : NOOP

  return {
    and: function(spec) {
      return AndSpecification(this, spec)
    },
    or: function(spec) {
      return OrSpecification(this, spec)
    },
    not: function() {
      return NotSpecification(this)
    },
    isSatisfiedBy: validIsSatisfiedBy
  }
}

///////////////////////////////////////////////////////
/////////// EXPORTS
///////////////////////////////////////////////////////
module.exports = ModuleFactoryPattern.factory(Specification)

///////////////////////////////////////////////////////
/////////// PRIVATE
///////////////////////////////////////////////////////
var NOOP = function() {
  return false
}
