/* jshint node: true */

'use strict'

var expect = require('chai').expect

var Specification = require('../specification').Constructor

var SpecificationTestToolbox = require('../../__test/toolbox')

describe('Specification Pattern || Basic Specification', function() {
  it('Create a valid specification', function() {
    var alwaysTrue = Specification(function() {
      return true
    })

    var spec = alwaysTrue
    var obj = 'whatever'
    var validateResult = function(result) {
      expect(result).to.be.equal(true)
    }
    SpecificationTestToolbox.validateSpecification(spec, obj, validateResult)
  })

  it('Combining specifications with OR from a Basic Specification', function() {
    var alwaysTrue = Specification(function() {
      return true
    })

    var alwaysFalse = Specification(function() {
      return false
    })

    // CASE 1 ///////////////////////////////////////////////
    var trueOrFalse = alwaysTrue.or(alwaysFalse)

    var spec = trueOrFalse
    var obj = 'whatever'
    var validateResult = function(result) {
      expect(result).to.be.equal(true || false)
    }
    SpecificationTestToolbox.validateSpecification(spec, obj, validateResult)

    // CASE 2 ///////////////////////////////////////////////
    var falseOrTrue = alwaysFalse.or(alwaysTrue)

    spec = falseOrTrue
    obj = 'whatever'
    validateResult = function(result) {
      expect(result).to.be.equal(false || true)
    }

    SpecificationTestToolbox.validateSpecification(spec, obj, validateResult)

    // CASE 3 ///////////////////////////////////////////////
    var trueOrTrue = alwaysTrue.or(alwaysTrue)

    spec = trueOrTrue
    obj = 'whatever'
    validateResult = function(result) {
      expect(result).to.be.equal(true || true)
    }
    SpecificationTestToolbox.validateSpecification(spec, obj, validateResult)

    // CASE 4 ///////////////////////////////////////////////
    var falseOrFalse = alwaysFalse.or(alwaysFalse)

    spec = falseOrFalse
    obj = 'whatever'
    validateResult = function(result) {
      expect(result).to.be.equal(false || false)
    }
    SpecificationTestToolbox.validateSpecification(spec, obj, validateResult)
  })

  it('Combining specifications with AND from a Basic Specification', function() {
    var alwaysTrue = Specification(function() {
      return true
    })

    var alwaysFalse = Specification(function() {
      return false
    })

    // CASE 1 ///////////////////////////////////////////////
    var trueAndFalse = alwaysTrue.and(alwaysFalse)

    var spec = trueAndFalse
    var obj = 'whatever'
    var validateResult = function(result) {
      expect(result).to.be.equal(true && false)
    }
    SpecificationTestToolbox.validateSpecification(spec, obj, validateResult)

    // CASE 2 ///////////////////////////////////////////////
    var falseAndTrue = alwaysFalse.and(alwaysTrue)

    spec = falseAndTrue
    obj = 'whatever'
    validateResult = function(result) {
      expect(result).to.be.equal(false && true)
    }
    SpecificationTestToolbox.validateSpecification(spec, obj, validateResult)

    // CASE 3 ///////////////////////////////////////////////
    var trueAndTrue = alwaysTrue.and(alwaysTrue)

    spec = trueAndTrue
    obj = 'whatever'
    validateResult = function(result) {
      expect(result).to.be.equal(true && true)
    }
    SpecificationTestToolbox.validateSpecification(spec, obj, validateResult)

    // CASE 4 ///////////////////////////////////////////////
    var falseAndFalse = alwaysFalse.and(alwaysFalse)

    spec = falseAndFalse
    obj = 'whatever'
    validateResult = function(result) {
      expect(result).to.be.equal(false && false)
    }
    SpecificationTestToolbox.validateSpecification(spec, obj, validateResult)
  })

  it('Negate a Basic Specification', function() {
    var alwaysTrue = Specification(function() {
      return true
    })

    var notAlwaysTrue = alwaysTrue.not()

    var spec = notAlwaysTrue
    var obj = 'whatever'
    var validateResult = function(result) {
      expect(result).to.be.equal(false)
    }
    SpecificationTestToolbox.validateSpecification(spec, obj, validateResult)
  })
})
