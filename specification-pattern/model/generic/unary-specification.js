/* jshint node: true */

'use strict'

///////////////////////////////////////////////////////
/////////// DEPENDENCIES
///////////////////////////////////////////////////////
var Promise = require('bluebird')

var PATH_TO_MODULE_FACTORY_PATTERN = '../../../module-factory-pattern'
var ModuleFactoryPattern = require(PATH_TO_MODULE_FACTORY_PATTERN + '/module-factory-pattern').Constructor()

///////////////////////////////////////////////////////
/////////// CONSTRUCTOR
///////////////////////////////////////////////////////
var UnarySpecification = function(spec1, evaluate, defaultResult) {
  var Specification = require('../specification').Constructor

  var unarySpecfication = Specification(function(obj) {
    var result = defaultResult === true

    if (spec1 && typeof spec1.isSatisfiedBy === 'function') {
      try {
        result = spec1.isSatisfiedBy(obj)
      } catch (e) {
        result = false
      }
    }

    if (result !== null && typeof result !== 'undefined' && typeof result.then === 'function') {
      var promise = result
      return promise
        .then(function(result) {
          evaluate(result)
        })
        .catch(function() {
          return Promise.resolve(defaultResult)
        })
    } else {
      return evaluate(result)
    }
  })

  return unarySpecfication
}

///////////////////////////////////////////////////////
/////////// EXPORTS
///////////////////////////////////////////////////////
module.exports = ModuleFactoryPattern.factory(UnarySpecification)

///////////////////////////////////////////////////////
/////////// MISC
///////////////////////////////////////////////////////
