/* jshint node: true */

'use strict'

///////////////////////////////////////////////////////
/////////// DEPENDENCIES
///////////////////////////////////////////////////////
var Promise = require('bluebird')

var PATH_TO_MODULE_FACTORY_PATTERN = '../../../module-factory-pattern'
var ModuleFactoryPattern = require(PATH_TO_MODULE_FACTORY_PATTERN + '/module-factory-pattern').Constructor()

///////////////////////////////////////////////////////
/////////// CONSTRUCTOR
///////////////////////////////////////////////////////
var BinarySpecification = function(spec1, spec2, evaluate, defaultResult) {
  var validDefaultResult = defaultResult === true
  var Specification = require('../specification').Constructor

  var binarySpecfication = Specification(function(obj) {
    return handleSpec(spec1, obj, validDefaultResult, function(spec1Sat) {
      return handleSpec(spec2, obj, validDefaultResult, function(spec2Sat) {
        return evaluate(spec1Sat, spec2Sat)
      })
    })
  })

  return binarySpecfication
}

///////////////////////////////////////////////////////
/////////// EXPORTS
///////////////////////////////////////////////////////
module.exports = ModuleFactoryPattern.factory(BinarySpecification)

///////////////////////////////////////////////////////
/////////// PRIVATE
///////////////////////////////////////////////////////
var handleSpec = function(spec, obj, defaultResult, next) {
  var result = defaultResult

  if (spec && typeof spec.isSatisfiedBy === 'function') {
    try {
      result = spec.isSatisfiedBy(obj)
    } catch (e) {
      result = false
    }
  }

  if (result !== null && typeof result !== 'undefined' && typeof result.then === 'function') {
    var promise = result
    return promise
      .then(function(result) {
        return next(result)
      })
      .catch(function(e) {
        console.error(e)
        return Promise.resolve(defaultResult)
      })
  } else {
    return next(result)
  }
}
