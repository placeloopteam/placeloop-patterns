/* jshint node: true */

'use strict'

///////////////////////////////////////////////////////
/////////// DEPENDENCIES
///////////////////////////////////////////////////////
var PATH_TO_MODULE_FACTORY_PATTERN = '../../../module-factory-pattern'
var ModuleFactoryPattern = require(PATH_TO_MODULE_FACTORY_PATTERN + '/module-factory-pattern').Constructor()

///////////////////////////////////////////////////////
/////////// CONSTRUCTOR
///////////////////////////////////////////////////////
var AndSpecification = function(spec1, spec2) {
  var BinarySpecification = require('../generic/binary-specification').Constructor

  var evaluate = AND
  var defaultResult = false
  var andSpecfication = BinarySpecification(spec1, spec2, evaluate, defaultResult)

  return andSpecfication
}

///////////////////////////////////////////////////////
/////////// EXPORTS
///////////////////////////////////////////////////////
module.exports = ModuleFactoryPattern.factory(AndSpecification)

///////////////////////////////////////////////////////
/////////// PRIVATE
///////////////////////////////////////////////////////
var AND = function(resultSpec1, resultSpec2) {
  return resultSpec1 && resultSpec2
}
