/* jshint node: true */

'use strict'

var expect = require('chai').expect

var Specification = require('../../specification').Constructor

var OrSpecification = require('../or-specification').Constructor
var AndSpecification = require('../and-specification').Constructor
var NotSpecification = require('../not-specification').Constructor

var SpecificationTestToolbox = require('../../../__test/toolbox')

describe('Specification Pattern || Logic Specification', function() {
  it('Create a valid OR Specification', function() {
    var alwaysTrue = Specification(function() {
      return true
    })

    var alwaysFalse = Specification(function() {
      return false
    })

    // CASE 1 ///////////////////////////////////////////////
    var trueOrFalse = OrSpecification(alwaysTrue, alwaysFalse)

    var spec = trueOrFalse
    var obj = 'whatever'
    var validateResult = function(result) {
      expect(result).to.be.equal(true || false)
    }
    SpecificationTestToolbox.validateSpecification(spec, obj, validateResult)

    // CASE 2 ///////////////////////////////////////////////
    var falseOrTrue = OrSpecification(alwaysFalse, alwaysTrue)

    spec = falseOrTrue
    obj = 'whatever'
    validateResult = function(result) {
      expect(result).to.be.equal(false || true)
    }
    SpecificationTestToolbox.validateSpecification(spec, obj, validateResult)

    // CASE 3 ///////////////////////////////////////////////
    var trueOrTrue = OrSpecification(alwaysTrue, alwaysTrue)

    spec = trueOrTrue
    obj = 'whatever'
    validateResult = function(result) {
      expect(result).to.be.equal(true || true)
    }
    SpecificationTestToolbox.validateSpecification(spec, obj, validateResult)

    // CASE 4 ///////////////////////////////////////////////
    var falseOrFalse = OrSpecification(alwaysFalse, alwaysFalse)

    spec = falseOrFalse
    obj = 'whatever'
    validateResult = function(result) {
      expect(result).to.be.equal(false || false)
    }
    SpecificationTestToolbox.validateSpecification(spec, obj, validateResult)
  })

  it('Combining specifications with AND from a Basic Specification', function() {
    var alwaysTrue = Specification(function() {
      return true
    })

    var alwaysFalse = Specification(function() {
      return false
    })

    // CASE 1 ///////////////////////////////////////////////
    var trueAndFalse = AndSpecification(alwaysTrue, alwaysFalse)

    var spec = trueAndFalse
    var obj = 'whatever'
    var validateResult = function(result) {
      expect(result).to.be.equal(true && false)
    }
    SpecificationTestToolbox.validateSpecification(spec, obj, validateResult)

    // CASE 2 ///////////////////////////////////////////////
    var falseAndTrue = AndSpecification(alwaysFalse, alwaysTrue)

    spec = falseAndTrue
    obj = 'whatever'
    validateResult = function(result) {
      expect(result).to.be.equal(false && true)
    }
    SpecificationTestToolbox.validateSpecification(spec, obj, validateResult)

    // CASE 3 ///////////////////////////////////////////////
    var trueAndTrue = AndSpecification(alwaysTrue, alwaysTrue)

    spec = trueAndTrue
    obj = 'whatever'
    validateResult = function(result) {
      expect(result).to.be.equal(true && true)
    }
    SpecificationTestToolbox.validateSpecification(spec, obj, validateResult)

    // CASE 4 ///////////////////////////////////////////////
    var falseAndFalse = AndSpecification(alwaysFalse, alwaysFalse)

    spec = falseAndFalse
    obj = 'whatever'
    validateResult = function(result) {
      expect(result).to.be.equal(false && false)
    }
    SpecificationTestToolbox.validateSpecification(spec, obj, validateResult)
  })

  it('Negate a Basic Specification', function() {
    var alwaysTrue = Specification(function() {
      return true
    })

    var notAlwaysTrue = NotSpecification(alwaysTrue)

    var spec = notAlwaysTrue
    var obj = 'whatever'
    var validateResult = function(result) {
      expect(result).to.be.equal(false)
    }
    SpecificationTestToolbox.validateSpecification(spec, obj, validateResult)
  })
})
