/* jshint node: true */

'use strict'

///////////////////////////////////////////////////////
/////////// DEPENDENCIES
///////////////////////////////////////////////////////
var PATH_TO_MODULE_FACTORY_PATTERN = '../../../module-factory-pattern'
var ModuleFactoryPattern = require(PATH_TO_MODULE_FACTORY_PATTERN + '/module-factory-pattern').Constructor()

///////////////////////////////////////////////////////
/////////// CONSTRUCTOR
///////////////////////////////////////////////////////
var OrSpecification = function(spec1, spec2) {
  var BinarySpecification = require('../generic/binary-specification').Constructor

  var evaluate = OR
  var defaultResult = false
  var orSpecfication = BinarySpecification(spec1, spec2, evaluate, defaultResult)

  return orSpecfication
}

///////////////////////////////////////////////////////
/////////// EXPORTS
///////////////////////////////////////////////////////
module.exports = ModuleFactoryPattern.factory(OrSpecification)

///////////////////////////////////////////////////////
/////////// PRIVATE
///////////////////////////////////////////////////////
var OR = function(resultSpec1, resultSpec2) {
  return resultSpec1 || resultSpec2
}
