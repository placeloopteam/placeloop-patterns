/* jshint node: true */

'use strict'

var MPSpec = require('../multipurpose-specification').Constructor()

var SpecificationTestToolbox = require('../../__test/toolbox')

var ExpectedResultFactory = {
  createAllFalse: function() {
    return {
      withNull: false,
      withUndefined: false,
      withNaN: false,
      withSimpleAndEmptyObject: false,
      withEmptyArray: false,
      withFunction: false,
      withEmptyString: false,
      withNonEmptyString: false,
      withIntegerAsString: false,
      withFloatAsString: false,
      withZero: false,
      withNegativeInteger: false,
      withPositiveInteger: false,
      withNegativeFloat: false,
      withPositiveFloat: false,
      withMaxValue: false,
      withInfinity: false,
      withNegativeInfinity: false,
      withTrue: false,
      withFalse: false,
      withError: false,
      withReferenceError: false
    }
  },
  createAllTrue: function() {
    var expectedResults = this.createAllFalse()
    for (var att in expectedResults) {
      if (expectedResults.hasOwnProperty(att)) {
        var expectedResultName = att
        expectedResults[expectedResultName] = true
      }
    }
    return expectedResults
  }
}

describe('Specification Pattern || Multipurpose Specification', function() {
  it('Is null', function() {
    var expectedResults = ExpectedResultFactory.createAllFalse()
    expectedResults.withNull = true

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(MPSpec.IS_NULL, expectedResults)
  })

  it('Is not null', function() {
    var expectedResults = ExpectedResultFactory.createAllTrue()
    expectedResults.withNull = false

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(MPSpec.IS_NOT_NULL, expectedResults)
  })

  it('Is undefined', function() {
    var expectedResults = ExpectedResultFactory.createAllFalse()
    expectedResults.withUndefined = true

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(MPSpec.IS_UNDEFINED, expectedResults)
  })

  it('Is not undefined', function() {
    var expectedResults = ExpectedResultFactory.createAllTrue()
    expectedResults.withUndefined = false

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(MPSpec.IS_NOT_UNDEFINED, expectedResults)
  })

  it('Is defined', function() {
    var expectedResults = ExpectedResultFactory.createAllTrue()
    expectedResults.withNull = false
    expectedResults.withUndefined = false

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(MPSpec.IS_DEFINED, expectedResults)
  })

  it('Is not defined', function() {
    var expectedResults = ExpectedResultFactory.createAllFalse()
    expectedResults.withNull = true
    expectedResults.withUndefined = true

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(MPSpec.IS_NOT_DEFINED, expectedResults)
  })

  it('Is an string', function() {
    var expectedResults = ExpectedResultFactory.createAllFalse()
    expectedResults.withEmptyString = true
    expectedResults.withNonEmptyString = true
    expectedResults.withIntegerAsString = true
    expectedResults.withFloatAsString = true

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(MPSpec.IS_AN_STRING, expectedResults)
  })

  it('Is not an string', function() {
    var expectedResults = ExpectedResultFactory.createAllTrue()
    expectedResults.withEmptyString = false
    expectedResults.withNonEmptyString = false
    expectedResults.withIntegerAsString = false
    expectedResults.withFloatAsString = false

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(MPSpec.IS_NOT_AN_STRING, expectedResults)
  })

  it('Is a blanc string', function() {
    var expectedResults = ExpectedResultFactory.createAllFalse()
    expectedResults.withEmptyString = true

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(MPSpec.IS_A_BLANC_STRING, expectedResults)
  })

  it('Is not a blanc string', function() {
    var expectedResults = ExpectedResultFactory.createAllFalse()
    expectedResults.withNonEmptyString = true
    expectedResults.withIntegerAsString = true
    expectedResults.withFloatAsString = true

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(
      MPSpec.IS_NOT_A_BLANC_STRING,
      expectedResults
    )
  })

  it('Is an array', function() {
    var expectedResults = ExpectedResultFactory.createAllFalse()
    expectedResults.withEmptyArray = true

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(MPSpec.IS_AN_ARRAY, expectedResults)
  })

  it('Is not an array', function() {
    var expectedResults = ExpectedResultFactory.createAllTrue()
    expectedResults.withEmptyArray = false

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(MPSpec.IS_NOT_AN_ARRAY, expectedResults)
  })

  it('Is an object', function() {
    var expectedResults = ExpectedResultFactory.createAllFalse()
    expectedResults.withSimpleAndEmptyObject = true
    expectedResults.withEmptyArray = true
    expectedResults.withError = true
    expectedResults.withReferenceError = true

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(MPSpec.IS_AN_OBJECT, expectedResults)
  })

  it('Is not an object', function() {
    var expectedResults = ExpectedResultFactory.createAllTrue()
    expectedResults.withSimpleAndEmptyObject = false
    expectedResults.withEmptyArray = false
    expectedResults.withError = false
    expectedResults.withReferenceError = false

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(MPSpec.IS_NOT_AN_OBJECT, expectedResults)
  })

  it('Is a simple object', function() {
    var expectedResults = ExpectedResultFactory.createAllFalse()
    expectedResults.withSimpleAndEmptyObject = true

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(MPSpec.IS_A_SIMPLE_OBJECT, expectedResults)
  })

  it('Is not a simple object', function() {
    var expectedResults = ExpectedResultFactory.createAllTrue()
    expectedResults.withSimpleAndEmptyObject = false

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(
      MPSpec.IS_NOT_A_SIMPLE_OBJECT,
      expectedResults
    )
  })

  it('Is a function', function() {
    var expectedResults = ExpectedResultFactory.createAllFalse()
    expectedResults.withFunction = true

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(MPSpec.IS_A_FUNCTION, expectedResults)
  })

  it('Is not a function', function() {
    var expectedResults = ExpectedResultFactory.createAllTrue()
    expectedResults.withFunction = false

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(MPSpec.IS_NOT_A_FUNCTION, expectedResults)
  })

  it('Is a number', function() {
    var expectedResults = ExpectedResultFactory.createAllFalse()
    expectedResults.withZero = true
    expectedResults.withNegativeInteger = true
    expectedResults.withPositiveInteger = true
    expectedResults.withNegativeFloat = true
    expectedResults.withPositiveFloat = true
    expectedResults.withMaxValue = true
    expectedResults.withInfinity = true
    expectedResults.withNegativeInfinity = true

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(MPSpec.IS_A_NUMBER, expectedResults)
  })

  it('Is not a number', function() {
    var expectedResults = ExpectedResultFactory.createAllTrue()
    expectedResults.withZero = false
    expectedResults.withNegativeInteger = false
    expectedResults.withPositiveInteger = false
    expectedResults.withNegativeFloat = false
    expectedResults.withPositiveFloat = false
    expectedResults.withMaxValue = false
    expectedResults.withInfinity = false
    expectedResults.withNegativeInfinity = false

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(MPSpec.IS_NOT_A_NUMBER, expectedResults)
  })

  it('Is NaN', function() {
    var expectedResults = ExpectedResultFactory.createAllTrue()
    expectedResults.withIntegerAsString = false
    expectedResults.withFloatAsString = false
    expectedResults.withZero = false
    expectedResults.withNegativeInteger = false
    expectedResults.withPositiveInteger = false
    expectedResults.withNegativeFloat = false
    expectedResults.withPositiveFloat = false
    expectedResults.withMaxValue = false
    expectedResults.withInfinity = false
    expectedResults.withNegativeInfinity = false

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(MPSpec.IS_NAN, expectedResults)
  })

  it('Is not a NaN', function() {
    var expectedResults = ExpectedResultFactory.createAllFalse()
    expectedResults.withIntegerAsString = true
    expectedResults.withFloatAsString = true
    expectedResults.withZero = true
    expectedResults.withNegativeInteger = true
    expectedResults.withPositiveInteger = true
    expectedResults.withNegativeFloat = true
    expectedResults.withPositiveFloat = true
    expectedResults.withMaxValue = true
    expectedResults.withInfinity = true
    expectedResults.withNegativeInfinity = true

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(MPSpec.IS_NOT_NAN, expectedResults)
  })

  it('Is a number as string', function() {
    var expectedResults = ExpectedResultFactory.createAllFalse()
    expectedResults.withIntegerAsString = true
    expectedResults.withFloatAsString = true

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(
      MPSpec.IS_A_NUMBER_AS_STRING,
      expectedResults
    )
  })

  it('Is not a number as string', function() {
    var expectedResults = ExpectedResultFactory.createAllTrue()
    expectedResults.withIntegerAsString = false
    expectedResults.withFloatAsString = false

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(
      MPSpec.IS_NOT_A_NUMBER_AS_STRING,
      expectedResults
    )
  })

  it('Is an integer', function() {
    var expectedResults = ExpectedResultFactory.createAllFalse()
    expectedResults.withZero = true
    expectedResults.withNegativeInteger = true
    expectedResults.withPositiveInteger = true
    expectedResults.withMaxValue = true

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(MPSpec.IS_AN_INTEGER, expectedResults)
  })

  it('Is not an integer', function() {
    var expectedResults = ExpectedResultFactory.createAllTrue()
    expectedResults.withZero = false
    expectedResults.withNegativeInteger = false
    expectedResults.withPositiveInteger = false
    expectedResults.withMaxValue = false

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(MPSpec.IS_NOT_AN_INTEGER, expectedResults)
  })

  it('Is a positive number', function() {
    var expectedResults = ExpectedResultFactory.createAllFalse()
    expectedResults.withZero = true
    expectedResults.withPositiveInteger = true
    expectedResults.withPositiveFloat = true
    expectedResults.withMaxValue = true
    expectedResults.withInfinity = true

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(
      MPSpec.IS_A_POSITIVE_NUMBER,
      expectedResults
    )
  })

  it('Is a positive integer', function() {
    var expectedResults = ExpectedResultFactory.createAllFalse()
    expectedResults.withZero = true
    expectedResults.withPositiveInteger = true
    expectedResults.withMaxValue = true

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(
      MPSpec.IS_A_POSITIVE_INTEGER,
      expectedResults
    )
  })

  it('Is a boolean', function() {
    var expectedResults = ExpectedResultFactory.createAllFalse()
    expectedResults.withTrue = true
    expectedResults.withFalse = true

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(MPSpec.IS_A_BOOLEAN, expectedResults)
  })

  it('Is not a boolean', function() {
    var expectedResults = ExpectedResultFactory.createAllTrue()
    expectedResults.withTrue = false
    expectedResults.withFalse = false

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(MPSpec.IS_NOT_A_BOOLEAN, expectedResults)
  })

  it('Is an error', function() {
    var expectedResults = ExpectedResultFactory.createAllFalse()
    expectedResults.withError = true
    expectedResults.withReferenceError = true

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(MPSpec.IS_AN_ERROR, expectedResults)
  })

  it('Is not an error', function() {
    var expectedResults = ExpectedResultFactory.createAllTrue()
    expectedResults.withError = false
    expectedResults.withReferenceError = false

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(MPSpec.IS_NOT_AN_ERROR, expectedResults)
  })

  it('Is a reference error', function() {
    var expectedResults = ExpectedResultFactory.createAllFalse()
    expectedResults.withReferenceError = true

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(
      MPSpec.IS_A_REFERENCE_ERROR,
      expectedResults
    )
  })

  it('Is not a reference error', function() {
    var expectedResults = ExpectedResultFactory.createAllTrue()
    expectedResults.withReferenceError = false

    SpecificationTestToolbox.validateSpecificationResultAgainstAllKinds(
      MPSpec.IS_NOT_A_REFERENCE_ERROR,
      expectedResults
    )
  })
})
