/* jshint node: true */

'use strict'

///////////////////////////////////////////////////////
/////////// DEPENDENCIES
///////////////////////////////////////////////////////
var PATH_TO_MODULE_FACTORY_PATTERN = '../../module-factory-pattern'
var ModuleFactoryPattern = require(PATH_TO_MODULE_FACTORY_PATTERN + '/module-factory-pattern').Constructor()

var PATH_TO_MODEL = '../model'
var Specification = require(PATH_TO_MODEL + '/specification').Constructor
var not = require(PATH_TO_MODEL + '/logic/not-specification').Constructor

///////////////////////////////////////////////////////
/////////// CONSTRUCTOR
///////////////////////////////////////////////////////
var MultipurposeSpecification = function() {
  ///////////////////////////////////////////////////////
  /////////// PRIVATE MEMBERS
  ///////////////////////////////////////////////////////

  return {
    ///////////////////////////////////////////////////////
    /////////// PUBLIC MEMBERS
    ///////////////////////////////////////////////////////
    IS_NULL: IS_NULL,
    IS_NOT_NULL: IS_NOT_NULL,

    IS_UNDEFINED: IS_UNDEFINED,
    IS_NOT_UNDEFINED: IS_NOT_UNDEFINED,

    IS_DEFINED: IS_DEFINED,
    IS_NOT_DEFINED: IS_NOT_DEFINED,

    IS_AN_STRING: IS_AN_STRING,
    IS_NOT_AN_STRING: IS_NOT_AN_STRING,

    IS_A_BLANC_STRING: IS_A_BLANC_STRING,
    IS_NOT_A_BLANC_STRING: IS_NOT_A_BLANC_STRING,

    IS_AN_ARRAY: IS_AN_ARRAY,
    IS_NOT_AN_ARRAY: IS_NOT_AN_ARRAY,

    IS_A_FUNCTION: IS_A_FUNCTION,
    IS_NOT_A_FUNCTION: IS_NOT_A_FUNCTION,

    IS_A_NUMBER: IS_A_NUMBER,
    IS_NOT_A_NUMBER: IS_NOT_A_NUMBER,

    IS_NAN: IS_NAN,
    IS_NOT_NAN: IS_NOT_NAN,

    IS_A_NUMBER_AS_STRING: IS_A_NUMBER_AS_STRING,
    IS_NOT_A_NUMBER_AS_STRING: IS_NOT_A_NUMBER_AS_STRING,

    IS_AN_INTEGER: IS_AN_INTEGER,
    IS_NOT_AN_INTEGER: IS_NOT_AN_INTEGER,

    IS_A_POSITIVE_NUMBER: IS_A_POSITIVE_NUMBER,
    IS_A_POSITIVE_INTEGER: IS_A_POSITIVE_INTEGER,

    IS_FINITE: IS_FINITE,
    IS_A_FINITE_NUMBER: IS_A_FINITE_NUMBER,
    IS_A_FINITE_POSITIVE_NUMBER: IS_A_FINITE_POSITIVE_NUMBER,

    IS_A_BOOLEAN: IS_A_BOOLEAN,
    IS_NOT_A_BOOLEAN: IS_NOT_A_BOOLEAN,

    IS_AN_ERROR: IS_AN_ERROR,
    IS_NOT_AN_ERROR: IS_NOT_AN_ERROR,

    IS_A_REFERENCE_ERROR: IS_A_REFERENCE_ERROR,
    IS_NOT_A_REFERENCE_ERROR: IS_NOT_A_REFERENCE_ERROR,

    IS_AN_OBJECT: IS_AN_OBJECT,
    IS_NOT_AN_OBJECT: IS_NOT_AN_OBJECT,

    IS_A_SIMPLE_OBJECT: IS_A_SIMPLE_OBJECT,
    IS_NOT_A_SIMPLE_OBJECT: IS_NOT_A_SIMPLE_OBJECT
  }
}

///////////////////////////////////////////////////////
/////////// EXPORTS
///////////////////////////////////////////////////////
module.exports = ModuleFactoryPattern.singleton(MultipurposeSpecification)

///////////////////////////////////////////////////////
/////////// MISC
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
/////////// NULL VALUES
var IS_NULL = Specification(function(value) {
  return value === null
})
var IS_NOT_NULL = not(IS_NULL)
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
/////////// DEFINED VALUES
var IS_UNDEFINED = Specification(function(value) {
  return typeof value === 'undefined'
})
var IS_NOT_UNDEFINED = not(IS_UNDEFINED)

var IS_DEFINED = IS_NOT_NULL.and(IS_NOT_UNDEFINED)
var IS_NOT_DEFINED = not(IS_DEFINED)
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
/////////// STRINGS VALUES
var IS_AN_STRING_UNSAFE = Specification(function(value) {
  return typeof value === 'string'
})
var IS_AN_STRING = IS_NOT_NULL.and(IS_AN_STRING_UNSAFE)
var IS_NOT_AN_STRING = not(IS_AN_STRING)

var IS_A_BLANC_STRING_UNSAFE = Specification(function(value) {
  return value.trim() === ''
})
var IS_A_BLANC_STRING = IS_AN_STRING.and(IS_A_BLANC_STRING_UNSAFE)
var IS_NOT_A_BLANC_STRING = IS_AN_STRING.and(not(IS_A_BLANC_STRING_UNSAFE))
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
/////////// ARRAY VALUES
var IS_AN_ARRAY_UNSAFE = Specification(function(value) {
  return Array.isArray(value)
})
var IS_AN_ARRAY = IS_NOT_NULL.and(IS_AN_ARRAY_UNSAFE)
var IS_NOT_AN_ARRAY = not(IS_AN_ARRAY)
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
/////////// FUNCTION VALUES
var IS_A_FUNCTION_UNSAFE = Specification(function(value) {
  return typeof value === 'function'
})
var IS_A_FUNCTION = IS_NOT_NULL.and(IS_A_FUNCTION_UNSAFE)
var IS_NOT_A_FUNCTION = not(IS_A_FUNCTION)
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
/////////// BOOLEAN VALUES
var IS_A_BOOLEAN_UNSAFE = Specification(function(value) {
  return typeof value === 'boolean'
})
var IS_A_BOOLEAN = IS_NOT_NULL.and(IS_A_BOOLEAN_UNSAFE)
var IS_NOT_A_BOOLEAN = not(IS_A_BOOLEAN)
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
/////////// ERRORS VALUES
var IS_AN_ERROR_UNSAFE = Specification(function(value) {
  return value instanceof Error
})
var IS_AN_ERROR = IS_NOT_NULL.and(IS_AN_ERROR_UNSAFE)
var IS_NOT_AN_ERROR = not(IS_AN_ERROR)

var IS_A_REFERENCE_ERROR_UNSAFE = Specification(function(value) {
  return value instanceof ReferenceError
})
var IS_A_REFERENCE_ERROR = IS_NOT_NULL.and(IS_A_REFERENCE_ERROR_UNSAFE)
var IS_NOT_A_REFERENCE_ERROR = not(IS_A_REFERENCE_ERROR)
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
/////////// OBJECT VALUES
var IS_AN_OBJECT_UNSAFE = Specification(function(value) {
  return typeof value === 'object'
})
var IS_AN_OBJECT = IS_NOT_NULL.and(IS_AN_OBJECT_UNSAFE)
var IS_NOT_AN_OBJECT = not(IS_AN_OBJECT)
var IS_A_SIMPLE_OBJECT = IS_AN_OBJECT.and(IS_NOT_AN_ARRAY).and(IS_NOT_AN_ERROR)
var IS_NOT_A_SIMPLE_OBJECT = not(IS_A_SIMPLE_OBJECT)
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
/////////// NaN VALUES (This fix some consistency issues of the isNaN function)
var IS_NAN_UNSAFE = Specification(function(value) {
  return isNaN(value)
})
var IS_NAN = IS_NOT_DEFINED.or(IS_AN_ARRAY)
  .or(IS_A_BOOLEAN)
  .or(IS_A_BLANC_STRING)
  .or(IS_NAN_UNSAFE)
var IS_NOT_NAN = not(IS_NAN)
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
/////////// NUMBER VALUES
var IS_A_NUMBER_AS_STRING = IS_NOT_A_BLANC_STRING.and(IS_NOT_NAN)
var IS_NOT_A_NUMBER_AS_STRING = not(IS_A_NUMBER_AS_STRING)

var IS_A_NUMBER_UNSAFE = Specification(function(value) {
  return typeof value === 'number'
})
var IS_A_NUMBER = IS_NOT_NULL.and(IS_A_NUMBER_UNSAFE).and(IS_NOT_NAN)
var IS_NOT_A_NUMBER = not(IS_A_NUMBER)

var IS_AN_INTEGER_UNSAFE = Specification(function(value) {
  return value % 1 === 0
})
var IS_AN_INTEGER = IS_A_NUMBER.and(IS_AN_INTEGER_UNSAFE)
var IS_NOT_AN_INTEGER = not(IS_AN_INTEGER)

var IS_A_POSITIVE_NUMBER_UNSAFE = Specification(function(value) {
  return value >= 0
})
var IS_A_POSITIVE_NUMBER = IS_A_NUMBER.and(IS_A_POSITIVE_NUMBER_UNSAFE)
var IS_A_POSITIVE_INTEGER = IS_AN_INTEGER.and(IS_A_POSITIVE_NUMBER)

var IS_FINITE = Specification(function(value) {
  return isFinite(value)
})

var IS_A_FINITE_NUMBER = IS_A_NUMBER.and(IS_FINITE)
var IS_A_FINITE_POSITIVE_NUMBER = IS_A_POSITIVE_NUMBER.and(IS_FINITE)

///////////////////////////////////////////////////////
