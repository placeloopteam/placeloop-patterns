/* jshint node: true */

'use strict'

var expect = require('chai').expect

var validateSpecification = function(spec, obj, validateResult, done) {
  // mocha done() is optional because the done() call can be executed by the invoker

  // Validating the spec structure
  validateSpecificationStructure(spec)

  // Apply the specification
  var specResult = spec.isSatisfiedBy(obj)

  // Validating the result value
  if (specResult !== null && typeof specResult !== 'undefined' && typeof specResult.then === 'function') {
    var promise = specResult

    expect(done).to.be.a('function')

    return promise
      .then(function(specResult) {
        validateResult(specResult)
      })
      .then(done)
      .catch(done)
  } else {
    validateResult(specResult)
    if (done !== null && typeof done === 'function') {
      done()
    }
  }
}

var validateSpecificationStructure = function(spec) {
  // And/Or/Not builders
  expect(spec.and).to.be.a('function')
  expect(spec.or).to.be.a('function')
  expect(spec.not).to.be.a('function')

  // Apply function
  expect(spec.isSatisfiedBy).to.be.a('function')
}

var validateSpecificationResultAgainstAllKinds = function(spec, expectedResults) {
  expect(spec.isSatisfiedBy(null)).to.be.equal(expectedResults.withNull)

  expect(spec.isSatisfiedBy(undefined)).to.be.equal(expectedResults.withUndefined)

  expect(spec.isSatisfiedBy(NaN)).to.be.equal(expectedResults.withNaN)

  expect(spec.isSatisfiedBy({})).to.be.equal(expectedResults.withSimpleAndEmptyObject)

  expect(spec.isSatisfiedBy([])).to.be.equal(expectedResults.withEmptyArray)

  expect(spec.isSatisfiedBy(function() {})).to.be.equal(expectedResults.withFunction)

  expect(spec.isSatisfiedBy('')).to.be.equal(expectedResults.withEmptyString)

  expect(spec.isSatisfiedBy('not-empty')).to.be.equal(expectedResults.withNonEmptyString)

  expect(spec.isSatisfiedBy('1.1')).to.be.equal(expectedResults.withIntegerAsString)

  expect(spec.isSatisfiedBy('1.1')).to.be.equal(expectedResults.withFloatAsString)

  expect(spec.isSatisfiedBy(0)).to.be.equal(expectedResults.withZero)

  expect(spec.isSatisfiedBy(-1)).to.be.equal(expectedResults.withNegativeInteger)

  expect(spec.isSatisfiedBy(1)).to.be.equal(expectedResults.withPositiveInteger)

  expect(spec.isSatisfiedBy(-1.1)).to.be.equal(expectedResults.withNegativeFloat)

  expect(spec.isSatisfiedBy(1.1)).to.be.equal(expectedResults.withPositiveFloat)

  expect(spec.isSatisfiedBy(Number.MAX_VALUE)).to.be.equal(expectedResults.withMaxValue)

  expect(spec.isSatisfiedBy(Infinity)).to.be.equal(expectedResults.withInfinity)

  expect(spec.isSatisfiedBy(-Infinity)).to.be.equal(expectedResults.withNegativeInfinity)

  expect(spec.isSatisfiedBy(true)).to.be.equal(expectedResults.withTrue)

  expect(spec.isSatisfiedBy(false)).to.be.equal(expectedResults.withFalse)

  expect(spec.isSatisfiedBy(new Error())).to.be.equal(expectedResults.withError)

  expect(spec.isSatisfiedBy(new ReferenceError())).to.be.equal(expectedResults.withReferenceError)
}

module.exports.validateSpecification = validateSpecification
module.exports.validateSpecificationStructure = validateSpecificationStructure
module.exports.validateSpecificationResultAgainstAllKinds = validateSpecificationResultAgainstAllKinds
