/* jshint node: true */

'use strict'

///////////////////////////////////////////////////////
/////////// DEPENDENCIES
///////////////////////////////////////////////////////
var PATH_TO_MODULE_FACTORY_PATTERN = '../module-factory-pattern'
var ModuleFactoryPattern = require(PATH_TO_MODULE_FACTORY_PATTERN + '/module-factory-pattern').Constructor()

var ObjectFunctionsInterceptor = require('./interceptor-pattern-obj').Constructor

///////////////////////////////////////////////////////
/////////// CONSTRUCTOR
///////////////////////////////////////////////////////
var ObjectFunctionsInterceptorConstructor = function(construct, pre, post) {
  return intercept(construct, pre, post)
}

///////////////////////////////////////////////////////
/////////// EXPORTS
///////////////////////////////////////////////////////
module.exports = ModuleFactoryPattern.factory(ObjectFunctionsInterceptorConstructor)

///////////////////////////////////////////////////////
/////////// PRIVATE
///////////////////////////////////////////////////////
var intercept = function(construct, pre, post) {
  var intercepted = function() {
    var result = construct.apply(null, arguments)
    if (typeof result.then === 'function') {
      var promise = result
      return promise.then(function(object) {
        return ObjectFunctionsInterceptor(object, pre, post)
      })
    } else {
      var object = result
      return ObjectFunctionsInterceptor(object, pre, post)
    }
  }

  return intercepted
}
