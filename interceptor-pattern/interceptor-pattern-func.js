/* jshint node: true */

'use strict'

///////////////////////////////////////////////////////
/////////// DEPENDENCIES
///////////////////////////////////////////////////////
var PATH_TO_MODULE_FACTORY_PATTERN = '../module-factory-pattern'
var ModuleFactoryPattern = require(PATH_TO_MODULE_FACTORY_PATTERN + '/module-factory-pattern').Constructor()

///////////////////////////////////////////////////////
/////////// CONSTRUCTOR
///////////////////////////////////////////////////////
var FunctionInterceptor = function(func, obj, preAction, postAction) {
  return intercept(func, obj, assertValidAction(preAction), assertValidAction(postAction))
}

///////////////////////////////////////////////////////
/////////// EXPORTS
///////////////////////////////////////////////////////
module.exports = ModuleFactoryPattern.factory(FunctionInterceptor)

///////////////////////////////////////////////////////
/////////// PRIVATE
///////////////////////////////////////////////////////
var intercept = function(func, obj, preAction, postAction) {
  var intercepted = function() {
    var args = arguments
    var chain = [
      {obj: preAction, func: preAction.execute, enrichArgs: pushAtTheEnd},
      {obj: obj, func: func, enrichArgs: pushAtTheBeginning},
      {
        obj: postAction,
        func: postAction.execute,
        enrichArgs: NOOP
      }
    ]
    return execute(chain, args)
  }
  return intercepted
}

var execute = function(chain, args, previousResult) {
  var link = chain[0]
  if (typeof link === 'undefined') {
    return previousResult
  }

  var result = link.func.apply(link.obj, args)
  if (result !== null && typeof result !== 'undefined' && typeof result.then === 'function') {
    var promise = result
    return promise.then(function(result) {
      return handleResult(link, chain, args, result)
    })
  } else {
    return handleResult(link, chain, args, result)
  }
}

var handleResult = function(link, chain, args, result) {
  var enrichedArgs = link.enrichArgs(args, result)
  var remainingChain = chain.slice(1)
  return execute(remainingChain, enrichedArgs, result)
}

var pushAtTheEnd = function(args, result) {
  return addToArray(args, result, 'push')
}

var pushAtTheBeginning = function(args, result) {
  return addToArray(args, result, 'unshift')
}

var addToArray = function(args, result, operation) {
  var argsPlusResult = Array.prototype.slice.call(args)
  argsPlusResult[operation](result)
  return argsPlusResult
}

var NOOP = function() {}

var NOOP_ACTION = {
  execute: function() {
    return arguments[0]
  }
}
var assertValidAction = function(action) {
  var valid = NOOP_ACTION
  if (action !== null) {
    if (typeof action === 'function') {
      valid = {
        execute: action
      }
    } else if (typeof action === 'object' && typeof action.execute === 'function') {
      valid = action
    }
  }
  return valid
}
