/* jshint node: true */

'use strict'

///////////////////////////////////////////////////////
/////////// DEPENDENCIES
///////////////////////////////////////////////////////
var PATH_TO_MODULE_FACTORY_PATTERN = '../module-factory-pattern'
var ModuleFactoryPattern = require(PATH_TO_MODULE_FACTORY_PATTERN + '/module-factory-pattern').Constructor()

var FunctionInterceptor = require('./interceptor-pattern-func').Constructor

///////////////////////////////////////////////////////
/////////// CONSTRUCTOR
///////////////////////////////////////////////////////
var ObjectFunctionsInterceptor = function(obj, pre, post) {
  return intercept(obj, pre, post)
}

///////////////////////////////////////////////////////
/////////// EXPORTS
///////////////////////////////////////////////////////
module.exports = ModuleFactoryPattern.factory(ObjectFunctionsInterceptor)

///////////////////////////////////////////////////////
/////////// PRIVATE
///////////////////////////////////////////////////////
var intercept = function(obj, pre, post) {
  var intercepted = obj
  for (var att in obj) {
    if (typeof obj[att] === 'function') {
      var func = intercepted[att]
      var interceptedFunc = FunctionInterceptor(func, obj, pre, post)
      intercepted[att] = interceptedFunc
    }
  }
  return intercepted
}
