/* jshint node: true */

'use strict'

///////////////////////////////////////////////////////
/////////// DEPENDENCIES
///////////////////////////////////////////////////////
var Factory = require('./module-factory-pattern-factory').Constructor
var Singleton = require('./module-factory-pattern-singleton').Constructor

///////////////////////////////////////////////////////
/////////// EXPORTS
///////////////////////////////////////////////////////
module.exports.Constructor = function() {
  return ModuleFactoryPatternFactory.getInstance()
}

///////////////////////////////////////////////////////
/////////// CONSTRUCTOR
///////////////////////////////////////////////////////
var ModuleFactoryPattern = function() {
  return {
    factory: function(constructor) {
      return Factory(useRealOrStub(constructor))
    },
    singleton: function(constructor) {
      return Singleton(useRealOrStub(constructor))
    }
  }
}

///////////////////////////////////////////////////////
/////////// INSTANCES
///////////////////////////////////////////////////////
var ModuleFactoryPatternFactory = {
  instance: undefined,
  getInstance: function() {
    if (typeof this.instance === 'undefined' || this.instance === null) {
      this.instance = ModuleFactoryPattern()
    }
    return this.instance
  }
}

///////////////////////////////////////////////////////
/////////// MISC
///////////////////////////////////////////////////////
var useRealOrStub = function(constructor) {
  return constructor
}
