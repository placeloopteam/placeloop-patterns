/* jshint node: true */

'use strict'

///////////////////////////////////////////////////////
/////////// DEPENDENCIES
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
/////////// EXPORTS
///////////////////////////////////////////////////////
module.exports.Constructor = function(constructor) {
  return {
    instance: undefined,
    Constructor: function() {
      if (typeof this.instance === 'undefined' || this.instance === null) {
        this.instance = constructor.apply(null, arguments)
      }
      return this.instance
    }
  }
}
